import React from "react"
import { Select, FormControl, FormHelperText, InputLabel, TextField } from "@material-ui/core"
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    display: "flex",
    marginTop: theme.spacing.unit * 2,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
  textField: {
    display: "flex",
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  }
});

const ReduxFormSelect = withStyles(styles)(({ classes, input, label,  name, meta: { touched, invalid, error}, children, ...custom }) => (
  <FormControl 
    className={classes.formControl} 
    required={error && error !== null} 
    error={touched && invalid}
  >
    <InputLabel htmlFor={name}>
      {label}
    </InputLabel>
    <Select 
      className={classes.selectEmpty} 
      name={name}
      inputProps={{id: {name}}} 
      {...input} 
      {...custom}
    >
      {children}
    </Select>
    <FormHelperText>
      {touched && error}
    </FormHelperText>
  </FormControl>
))

const ReduxFormInput = withStyles(styles)(({ classes, name, label, placeholder, input, meta: { touched, invalid, error }, ...custom }) => (
  <TextField
    required={error && error !== null}
    error={touched && invalid}
    helperText={touched && error}
    label={label}
    placeholder={placeholder}
    name={name}
    className={classes.textField}
    margin="normal"
    {...input}
    {...custom}
  />
))


export {ReduxFormInput, ReduxFormSelect}