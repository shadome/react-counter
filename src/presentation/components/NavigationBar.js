import React, {Component} from "react"
import { NavLink } from "react-router-dom"

import { withStyles, AppBar, Toolbar, Grid, Typography, Button } from "@material-ui/core"

import { RegisterFoodPath } from "../pages/RegisterFood/RegisterFoodPage"
import { ListFoodDictionaryPath } from "../pages/ListFoodDictionary/ListFoodDictionaryPage"

// TODO display bug when a horizontal scroll appears: the colour band is not extended towards the right

class NavigationBar extends Component {
  render() {
    const { classes } = this.props
    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <Grid container>
              <Grid item md={2} />
              <Grid container item xs={12} md={8}>
                <Typography variant="h5" color="inherit" className={classes.grow}>
                  Counter {/* TODO generic link to app name */}
                </Typography>
                <Button color="inherit" component={NavLink} to={ListFoodDictionaryPath}>
                  Dictionary
                </Button>
                <Button color="inherit" component={NavLink} to={RegisterFoodPath}>
                  Register food
                </Button>
              </Grid>
              <Grid item md={2} />
            </Grid>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
}

export default withStyles(styles)(NavigationBar)