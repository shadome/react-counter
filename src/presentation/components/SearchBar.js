import React, { Component } from "react"
import PropTypes from "prop-types"
import { withStyles, Paper, InputBase, Divider, IconButton, Icon  } from "@material-ui/core"

const styles = {
  root: {
    padding: "2px 4px",
    display: "flex",
    alignItems: "center",
  },
  input: {
    marginLeft: 8,
    marginRight: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  divider: {
    width: 1,
    height: 28,
    margin: 4,
  },
}

class SearchBar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: props.initialText || ""
    }
  }
  handleChange(event) {
    this.setState({...this.state,
      value:event.target.value,
    })
    this.props.onChange(event.target.value)
  }
  clearInput() {
    this.handleChange({target: {value: ""}})  // TODO efficace mais bof
  }
  render() {
    const { classes } = this.props
    return (
      <Paper className={classes.root}>
        <Icon disabled className={classes.iconButton}>search</Icon>
        <InputBase 
          value={this.state.value} 
          onChange={this.handleChange.bind(this)} 
          className={classes.input} 
          placeholder="Search the dictionary" 
        />
        <Divider className={classes.divider} />
        <IconButton 
          onClick={this.clearInput.bind(this)} 
          className={classes.iconButton}
          disabled={!this.state.value}
        >
          <Icon>close</Icon>
        </IconButton>
      </Paper>
    )
  }
}

SearchBar.propTypes = {
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(SearchBar)