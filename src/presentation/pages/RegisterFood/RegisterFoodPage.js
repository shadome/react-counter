import React, { Component } from "react"
import { withStyles, Grid } from "@material-ui/core"

import RegisterFoodContainer from "./RegisterFoodContainer"

export const RegisterFoodPath = "/register-food"

class RegisterFoodPage extends Component {
  render() {
    const { classes, ...other } = this.props
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs />
          <Grid item xs={8}>
            <RegisterFoodContainer {...other} id={this.props.match.params.id} />
          </Grid>
          <Grid item xs />
        </Grid>
      </div>
    );
  }
}
const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 10,
  },
})

export default withStyles(styles)(RegisterFoodPage)