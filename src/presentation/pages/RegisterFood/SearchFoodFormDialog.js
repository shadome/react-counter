// TODO treat this as a real form with submission
import React, { Component } from "react"
import { withStyles,
  Button, Dialog, DialogActions, DialogContent, DialogTitle, DialogContentText,
} from "@material-ui/core"

import TextFieldAutocomplete from "../../components/TextFieldAutocomplete"

import { getEntry } from "../../../proxies/LocalDatabaseServices"
import { DATASOURCES_METADATA } from "../../../business/Metadata"

const styles = (theme) => ({
  dialogContent: {
    height: 400,
  },
  dialogContentText: {
    marginTop: theme.spacing.unit * 2,
  }
})

class SearchFoodFormDialog extends Component {
  state = {
    selectedFood: undefined
  }
  handleChangeSelect(food, idDatasource) {
    this.setState({...this.state, 
      selectedFood: food && getEntry(food.value, idDatasource)
    })
  }
  render() {
    const { selectedFood } = this.state
    const { classes, open, onSubmit, onCancel, idDatasource } = this.props
    const dataSource = DATASOURCES_METADATA[idDatasource] || {}
    return (
      <Dialog 
        open={open}
        onClose={onCancel}
      >
        <DialogTitle>
          Select a food from {dataSource.label}
        </DialogTitle>
        <DialogContent className={classes.dialogContent}>
          <TextFieldAutocomplete 
              onChangeSelect={(input) => this.handleChangeSelect(input, idDatasource)} 
              placeholder="Search for foods" 
          />
          <DialogContentText className={classes.dialogContentText}>
            {/* TODO */}
            TODO food recap before submit
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancel} color="primary">
            Cancel
          </Button>
          <Button disabled={!selectedFood} onClick={() => onSubmit(selectedFood.alim_code)} color="primary">
            Select
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

export default withStyles(styles)(SearchFoodFormDialog)