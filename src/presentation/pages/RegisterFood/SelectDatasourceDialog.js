import React, { Component } from "react"
import { withStyles, List, ListItem, ListItemIcon, ListItemText,
  Dialog, DialogContent, DialogTitle, Avatar,
} from "@material-ui/core"

import { DATASOURCES_METADATA } from "../../../business/Metadata"

const styles = (theme) => ({
})

class SelectDatasourceDialog extends Component {
  render() {
    const { open, onSubmit, onCancel } = this.props
    return (
      <div >
        <Dialog 
          open={open}
          onClose={onCancel}
        >
          <DialogTitle>Select a data source</DialogTitle>
          <DialogContent>
            <List>
              {Object.values(DATASOURCES_METADATA).map(x => (
                <ListItem button key={x.id} onClick={() => onSubmit(x.id)}>
                  <ListItemIcon>
                    <Avatar src={x.icon} />
                  </ListItemIcon>
                  <ListItemText primary={x.label} />
                </ListItem>)
              )}
            </List>
          </DialogContent>
        </Dialog>
      </div>
    )
  }
}

export default withStyles(styles)(SelectDatasourceDialog)