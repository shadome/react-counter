import React, { Component } from "react"
import { connect } from "react-redux"
import { Field, reduxForm, initialize, formValueSelector } from "redux-form"

import { withStyles, MenuItem, Button, Icon, Card, 
  CardContent, Typography, Grid, InputAdornment, Avatar,
} from "@material-ui/core"

import { CATEGORIES_METADATA, FOOD_METADATA } from "../../../business/Metadata"
import { ListFoodDictionaryPath } from "../ListFoodDictionary/ListFoodDictionaryPage"
import { putFood, postFood, initFood } from "../../../business/actions/FoodActions"
import { getEntry } from "../../../proxies/LocalDatabaseServices"
import { ReduxFormInput, ReduxFormSelect } from "../../components/ReduxFormComponents"
import SearchFoodFormDialog from "./SearchFoodFormDialog"
import SelectDatasourceDialog from "./SelectDatasourceDialog"

import * as FoodAdapters from "../../../business/adapters/FoodAdapters"
// @see https://redux-form.com/6.1.1/examples/material-ui/

// TODO add tab logic (?) for unitary vs composed entries (composed being based upon unitary values)

const FORM_ID = "registerFoodForm"

class RegisterFoodContainer extends Component {
  fieldGridProps = { xs: 12, sm: 6}
  constructor(props) {
    super(props)
    this.state = {
      openDatasourceDialog: false,
      openSelectDatasourceDialog: false,
    }
    if (props.id) { // modifying existing item
      props.init(props.id, (food) => props.dispatch(initialize(FORM_ID, food)))
    }
  }
  handleOpenSelectDatasourceDialog = () => {
    this.setState({...this.state, openSelectDatasourceDialog: true})
  }
  handleCloseSelectDatasourceDialog = () => {
    this.setState({...this.state, openSelectDatasourceDialog: false})
  }
  handleOpenDatasourceDialog = (idDatasource) => {
    this.setState({ ...this.state, 
      openDatasourceDialog: true, 
      openSelectDatasourceDialog: false,
      idDatasource: idDatasource,
    })
  }
  handleCloseDatasourceDialog = () => {
    this.setState({ ...this.state, 
      openDatasourceDialog: false,
      idDatasource: undefined
    })
  }
  handleSubmitDatasourceDialog(id) {
    const food = getEntry(id)
    this.props.dispatch(initialize(FORM_ID, FoodAdapters.proxyToBusiness(food)))
    this.setState({ ...this.state, openDatasourceDialog: false })
  }
  handleSubmit(food) {
    if (this.props.id) { // modifying existing item
      this.props.putFood(this.props.id, food)
    } else {
      this.props.postFood(food)
    }
    this.props.history.push(ListFoodDictionaryPath)
  }
  renderTextFieldInGridItemFromMeta(metadata, overrideGridItemProps) {
    const value = this.props.values[metadata.id]
    return (
      <Grid item {...this.fieldGridProps} {...overrideGridItemProps} container alignItems="baseline">
        {/* TODO async */}
        {metadata.symbol && 
          <Avatar>{metadata.symbol[0] === "B" ? 
            (<span>B<sub>{metadata.symbol.substring(1)}</sub></span>) : 
            metadata.symbol
            }</Avatar>
        }
        <Field type="text"
            style={{flex:1}}
            component={ReduxFormInput} 
            label={metadata.label} 
            name={metadata.id} 
            InputProps={metadata.unit && {
              endAdornment: (
                <InputAdornment position="end">
                  {metadata.unit.symbol}
                </InputAdornment>
              )
            }}
            helperText={!isNaN(value) && metadata.rdaLowerBound &&
              Math.round(value / metadata.rdaLowerBound * 100) + "% of RDI"
            }
        />
      </Grid>
    )
  }
  renderGeneralInformationCard() {
    const { classes } = this.props
    // TODO enhance UX
    const fieldGridPropsL = { xs: 12, sm: 6, md:9}
    const fieldGridPropsS = { xs: 12, sm: 6, md: 3}
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            General information
          </Typography>
          <Grid container alignItems="baseline">
            <Grid item md={9} sm={12} container alignItems="flex-start">
              {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.name, fieldGridPropsL)}
              <Grid item {...this.fieldGridProps} {...fieldGridPropsS}>
                <Field 
                    component={ReduxFormSelect} 
                    label={FOOD_METADATA.category.label} 
                    name={FOOD_METADATA.category.id} 
                >
                  <MenuItem disabled>
                    <em>Choose a category</em>
                  </MenuItem>
                  {Object.values(CATEGORIES_METADATA).map(x => (
                    <MenuItem key={x.id} value={x.id}>
                      {x.label}
                    </MenuItem>
                  ))}
                </Field>
              </Grid>
            </Grid>
            <Grid item {...fieldGridPropsS} >
              <Button 
                  onClick={this.handleOpenSelectDatasourceDialog.bind(this)} 
                  className={classes.button} 
                  variant="contained" 
                  color="primary"
                  size="large"
                >
                <Icon className={classes.leftIcon}>cloud_download</Icon>
                <Typography color="inherit" noWrap>load from database</Typography>
              </Button>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    );
  }
  renderMacronutrientsCard() {
    const { classes } = this.props
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            Macronutrients per 100g
          </Typography>
          <Grid container>
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.energy)}
            <Grid item {...this.fieldGridProps} />
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.proteins)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.carbohydrates)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.fat)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.alcohol)}
          </Grid>
        </CardContent>
      </Card>
    );
  }
  renderDetailedMacronutrientsCard() {
    const { classes } = this.props
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            Detailed macronutrients per 100g
          </Typography>
          <Grid container>
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.fibres)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.carbs_sugar)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.fat_saturated)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.fat_fa_n9)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.fat_fa_n6)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.fat_fa_n3)}
          </Grid>
        </CardContent>
      </Card>
    );
  }
  renderVitaminsCard() {
    const { classes } = this.props
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            Vitamins per 100g
          </Typography>
          <Grid container>
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.provit_a)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_c)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_d)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_e)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_k)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b1)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b2)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b3)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b5)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b6)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b7)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b9)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_b12)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.vit_choline)}
          </Grid>
        </CardContent>
      </Card>
    );
  }
  renderMineralsCard() {
    const { classes } = this.props
    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography color="textSecondary" gutterBottom>
            Vitamins per 100g
          </Typography>
          <Grid container>
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_ca)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_cl)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_cr)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_cu)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_fe)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_i)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_k)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_mg)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_mn)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_mo)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_na)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_p)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_se)}
            {this.renderTextFieldInGridItemFromMeta(FOOD_METADATA.min_zn)}
          </Grid>
        </CardContent>
      </Card>
    );
  }
  render() {
    const { classes, handleSubmit } = this.props
    const { openDatasourceDialog, openSelectDatasourceDialog } = this.state
    return (
      <div> 
        <SelectDatasourceDialog
          open={openSelectDatasourceDialog}
          onSubmit={this.handleOpenDatasourceDialog.bind(this)}
          onCancel={this.handleCloseSelectDatasourceDialog.bind(this)}
        />
        <SearchFoodFormDialog 
          open={openDatasourceDialog} 
          idDatasource={this.state.idDatasource}
          onSubmit={this.handleSubmitDatasourceDialog.bind(this)}
          onCancel={this.handleCloseDatasourceDialog.bind(this)} 
        />
        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
          <Grid container spacing={24}>
            <Grid item className={classes.gridItem} xs={12}>
              {this.renderGeneralInformationCard()}
            </Grid>
            <Grid item className={classes.gridItem} xs={12} lg={6}>
              {this.renderMacronutrientsCard()}
            </Grid>
            <Grid item className={classes.gridItem} xs={12} lg={6}>
              {this.renderDetailedMacronutrientsCard()}
            </Grid>
            <Grid item className={classes.gridItem} xs={12} lg={6}>
              {this.renderVitaminsCard()}
            </Grid>
            <Grid item className={classes.gridItem} xs={12} lg={6}>
              {this.renderMineralsCard()}
            </Grid>
          </Grid>
          <Grid container justify="flex-end" alignItems="baseline">
            <Button variant="contained" color="secondary"
                className={classes.button} 
                onClick={this.props.history.goBack}
            >
              <Icon className={classes.leftIcon}>clear</Icon>
              Cancel
            </Button>
            <Button variant="contained" color="primary" type="submit"
                className={classes.button} 
                disabled={this.props.invalid} 
            >
              <Icon className={classes.leftIcon}>send</Icon>
              Submit
            </Button>
          </Grid>
        </form>
      </div>
    );
  }
}

// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #67+: redux forms
// INCOMPLETE, refer to https://redux-form.com/
const fields = Object.values(FOOD_METADATA).map(x => x.id)

function validate(values) {
  const errors = {}
  fields.forEach(field => {
    if (FOOD_METADATA[field].required && !values[field]) {
      errors[field] = "Required"
    }
  })
  return errors
}

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  gridItem: {
    display: "flex",
  },
  card: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    marginBottom: theme.spacing.unit,
  }
})

const selector = formValueSelector(FORM_ID)

export default reduxForm({
  form: FORM_ID,
  fields: fields,
  validate: validate,
})(connect(
  state => ({
    getFoodPending: state.foodReducer.getFoodPending,
    getFoodError: state.foodReducer.getFoodError,
    postFoodPending: state.foodReducer.postFoodPending,
    postFoodError: state.foodReducer.postFoodError,
    values: selector(state, ...Object.keys(FOOD_METADATA)),
  }),
  (dispatch) => ({
    putFood: (id, food) => dispatch(putFood(id, food)),
    postFood: (food) => dispatch(postFood(food)),
    init: (foodId, callback) => dispatch(initFood(foodId, callback))
  })
)(withStyles(styles)(RegisterFoodContainer)))