import React, {Component} from "react"
import { withStyles, Grid } from "@material-ui/core"

import ListFoodDictionaryContainer from "./ListFoodDictionaryContainer"

export const ListFoodDictionaryPath = "/"

class ListFoodDictionaryPage extends Component {
  render() {
    const { classes, ...other } = this.props
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item md={2} />
          <Grid container item xs={12} md={8}>
            <ListFoodDictionaryContainer {...other} />
          </Grid>
          <Grid item md={2} />
        </Grid>
      </div>
    );
  }
}
const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: 10,
  },
})

export default withStyles(styles)(ListFoodDictionaryPage)