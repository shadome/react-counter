import React, {Component} from "react"
import { connect } from "react-redux"

import { 
  TableBody, TableCell, TableRow, IconButton, Icon, Tooltip, Typography,
} from "@material-ui/core"

import { RegisterFoodPath } from "../RegisterFood/RegisterFoodPage"
import { FOOD_METADATA } from "../../../business/Metadata";

class ListFoodDictionaryTableBodyContainer extends Component {
  handleModify(id) {
    this.props.history.push(`${RegisterFoodPath}/${id}`)
  }
  render() {
    const { foodList, optionalColumnKeys, onDelete } = this.props
    const columns = optionalColumnKeys.map(x => FOOD_METADATA[x])
    return (
        <TableBody>
          {(!foodList || foodList.length < 1) 
              ? <TableRow key={-1}>
                  <TableCell colSpan="999">No results.</TableCell>
                </TableRow> 
              : foodList.map(item => (
                <TableRow 
                  hover 
                  key={item.id}
                >
                  <TableCell component="th" scope="row">
                    <Typography>{item.name}</Typography> 
                    {/* TODO noWrap causes overflow, no good solution right now to extend the first column... */}
                    {/* {item.name} */}
                  </TableCell>
                  {columns.map(col => (
                    <TableCell key={`${item.id}-${col.id}`} align={col.unit ? "right" : "left"}>
                      {item[col.id]}
                    </TableCell>
                  ))}
                  <TableCell align="right" padding="none">
                    <Typography noWrap>
                      <Tooltip title="Modify this item" placement="top-end" enterDelay={300}>
                        <IconButton onClick={() => this.handleModify(item.id)}>
                          <Icon fontSize="small">create</Icon>
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Delete this item" placement="top-end" enterDelay={300}>
                        <IconButton onClick={() => onDelete(item)}>
                          <Icon fontSize="small">delete</Icon>
                        </IconButton>
                      </Tooltip>
                    </Typography>
                  </TableCell>
                </TableRow>
              )
          )}
        </TableBody>
    );
  }
}

export default connect(
  state => ({
    getFoodsPending: state.foodReducer.getFoodsPending,
    getFoodsError: state.foodReducer.getFoodsError,
    optionalColumnKeys: state.dictionaryReducer.optionalColumnKeys,
  }),
  (dispatch) => ({
  })
)(ListFoodDictionaryTableBodyContainer)