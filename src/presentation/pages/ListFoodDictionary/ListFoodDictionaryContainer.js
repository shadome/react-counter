import React, { Component } from "react"
import { connect } from "react-redux"

import { withStyles, Toolbar, Typography, Icon, Grid, Paper, IconButton,
  Table, TableCell, TableHead, TablePagination, TableRow, TableSortLabel, 
  Tooltip, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, Button 
} from "@material-ui/core";

import { FOOD_METADATA } from "../../../business/Metadata"
import { getFoods, deleteFood } from "../../../business/actions/FoodActions"
import { doSort, doFilter, doChangePage, doChangeNbRowsPerPage } from "../../../business/actions/DictionaryActions"

import SearchBar from "../../components/SearchBar"
import TableBodyContainer from "./ListFoodDictionaryTableBodyContainer"
import SelectColumnsDialog from "./SelectColumnsDialog"
// TODO persist state which must not be cleared when changing pages OR use URL params for pseudo statelessness
class ListFoodDictionaryContainer extends Component {
  constructor(props) {
    super(props)
    this.props.getFoods()
    this.state = {
      openSelectColumnsDialog: false,
      displayAlert: false,
    }
  }
  /* Column selection */
  handleOpenSelectColumnsDialog() {
    this.setState({...this.state, openSelectColumnsDialog: true})
  }
  handleCloseSelectColumnsDialog() {
    this.setState({...this.state, openSelectColumnsDialog: false})
  }
  /* Confirm dialog */
  handleDeleteIntent(item) {
    this.setState({...this.state, 
      displayAlert: true, 
      pendingDelete: item
    })
  }
  handleDelete(id) {
    this.setState({...this.state, 
      displayAlert: false,
      pendingDelete: undefined,
    })
    this.props.deleteFood(id)
  }
  handleClickNo() {
    this.setState({...this.state, 
      displayAlert: false,
      pendingDelete: undefined,
    })
  }
  /* Paging, sorting, filtering */
  applyFilter(list) {
    const { filterText } = this.props
    return filterText && filterText.length > 0
      ? list.filter(x => x.name.toUpperCase().indexOf(filterText.toUpperCase()) >= 0)
      : list
  }
  applySort(list) {
    const { idCol, orderDesc } = this.props
    const entry = FOOD_METADATA[idCol]
    return list.sort((a, b) => {
      return a[entry.id] === undefined && b[entry.id] === undefined ? 0 :
        a[entry.id] === undefined ? orderDesc ? 1 : -1 :
        b[entry.id] === undefined ? orderDesc ? -1 : 1 :
        entry.unit
          ? orderDesc ? b[entry.id] - a[entry.id] : a[entry.id] - b[entry.id]
          : orderDesc ? b[entry.id].localeCompare(a[entry.id]) : a[entry.id].localeCompare(b[entry.id])
    })
  }
  applyPaging(list) {
    const { idPage, nbRowsPerPage } = this.props
    const start = idPage * nbRowsPerPage
    return list.slice(start, start + nbRowsPerPage)
  }
  renderConfirmDialog() {
    const { pendingDelete, displayAlert } = this.state
    return (
    <Dialog open={displayAlert} onClose={this.handleClickNo.bind(this)}>
      <DialogTitle>{"Confirm"}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Do you want to delete the food item{pendingDelete && (<b> {pendingDelete.name}</b>)}?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => this.handleDelete(pendingDelete && pendingDelete.id)} color="primary">
          Yes
        </Button>
        <Button onClick={this.handleClickNo.bind(this)} color="primary" autoFocus>
          No
        </Button>
      </DialogActions>
    </Dialog>
    )
  }
  render() {
    const { classes, doSort, doChangeNbRowsPerPage, doChangePage, doFilter,
      idCol, orderDesc, nbRowsPerPage, idPage, foodList, optionalColumnKeys, filterText,
    } = this.props
    const { openSelectColumnsDialog } = this.state
    const order = orderDesc ? "desc" : "asc"
    let refinedFoodList = this.applyFilter(foodList)
    const listLength = refinedFoodList.length // length before paging
    refinedFoodList = this.applyPaging(this.applySort(refinedFoodList))
    const tableColumns = [
      FOOD_METADATA.name,
      ...optionalColumnKeys.map(x => FOOD_METADATA[x]),
      { id: "actions", unit: true, label: "Actions" } // unit to simulate numeric value
    ]
    const renderTableSortLabel = (x) => (
      <TableSortLabel
        active={idCol === x.id}
        direction={order}
        onClick={() => doSort(x.id)}
      >
        <Typography variant="inherit" >
          {x.label} 
          {x.unit && x.unit.symbol && (` (${x.unit.symbol})`)}
        </Typography>
      </TableSortLabel>
    )
    return (
      <div className={classes.root}>
        {this.renderConfirmDialog()}
        <SelectColumnsDialog 
          open={openSelectColumnsDialog}
          onClose={this.handleCloseSelectColumnsDialog.bind(this)} 
        />
        <SearchBar initialText={filterText} onChange={(text) => doFilter(text)} />
        <Paper className={classes.table}>
          <Toolbar>
            <Grid container justify="space-between" alignItems="baseline">
              <Grid item>
                <Typography variant="h6">
                  Food dictionary
                </Typography>
              </Grid>
              <Grid item>
                <Tooltip placement="top-end" title="Configure columns" enterDelay={300}>
                  <IconButton onClick={this.handleOpenSelectColumnsDialog.bind(this)}>
                    <Icon color="action">view_column</Icon>
                  </IconButton>
                </Tooltip>
              </Grid>
            </Grid>
          </Toolbar>
          <Table padding="checkbox">
            <TableHead>
              <TableRow>
                {tableColumns.map(x => (
                  <TableCell key={x.id}
                    align={x.unit ? "right" : "left"}
                    sortDirection={idCol === x.id ? order : false}
                  >
                  { x.description ? (
                    <Tooltip title={x.description} placement="top-end" enterDelay={300}>
                      {renderTableSortLabel(x)}
                    </Tooltip> 
                    ) : renderTableSortLabel(x) 
                  }
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBodyContainer 
              history={this.props.history} 
              onDelete={this.handleDeleteIntent.bind(this)} 
              foodList={refinedFoodList}
            />
          </Table>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={listLength}
            rowsPerPage={nbRowsPerPage}
            page={idPage}
            onChangePage={(event, idPage) => doChangePage(idPage)}
            onChangeRowsPerPage={(event) => doChangeNbRowsPerPage(event.target.value)}
          />      
        </Paper>
      </div>
    )
  }
}
const styles = theme => ({
  root: {
    flex:1, 
  },
  table: {
    marginTop: theme.spacing.unit,
  },
})

export default connect(
  state => ({
    foodList: state.foodReducer.foodList,
    deleteFoodPending: state.foodReducer.deleteFoodPending,
    deleteFoodError: state.foodReducer.deleteFoodError,
    idCol: state.dictionaryReducer.idCol,
    orderDesc: state.dictionaryReducer.orderDesc,
    nbRowsPerPage: state.dictionaryReducer.nbRowsPerPage,
    idPage: state.dictionaryReducer.idPage,
    optionalColumnKeys: state.dictionaryReducer.optionalColumnKeys,
    filterText: state.dictionaryReducer.filterText,
  }),
  (dispatch) => ({
    getFoods: () => dispatch(getFoods()),
    deleteFood: id => dispatch(deleteFood(id)),
    doSort: idCol => dispatch(doSort(idCol)),
    doChangePage: (idPage) => dispatch(doChangePage(idPage)),
    doChangeNbRowsPerPage: (nbRowsPerPage) => dispatch(doChangeNbRowsPerPage(nbRowsPerPage)),
    doFilter: filterText => dispatch(doFilter(filterText)),
  })
)(withStyles(styles)(ListFoodDictionaryContainer))