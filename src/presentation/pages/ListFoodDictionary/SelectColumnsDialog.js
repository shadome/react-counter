import React, { Component } from "react"
import { connect } from "react-redux"
import { withStyles, Grid,
  FormGroup, FormControlLabel, Checkbox,
  Dialog, DialogContent, DialogTitle, DialogActions, Button,
} from "@material-ui/core"

import { doSelectOptionalColumns } from "../../../business/actions/DictionaryActions"
import { INITIAL_STATE } from "../../../business/reducers/DictionaryReducer"
import { FOOD_METADATA } from "../../../business/Metadata"

const styles = (theme) => ({
})

class SelectColumnsDialog extends Component {
  constructor(props) {
    super(props)
    const { optionalColumnKeys } = props
    this.state = {
      checkedColumns: Object.values(FOOD_METADATA)
        .filter(x => x.id !== FOOD_METADATA.name.id)
        .map(x => x.id) // not using Object.keys to avoid unexpected keys and because O(2n) == O(n)
        .reduce((cols, colKey) => {cols[colKey] = optionalColumnKeys.includes(colKey) ; return cols}, {})
    }
  }
  handleReset() {
    const initialValues = INITIAL_STATE.optionalColumnKeys // is it rigorous?
    this.setState({...this.state,
      checkedColumns: Object.values(FOOD_METADATA)
        .filter(x => x.id !== FOOD_METADATA.name.id)
        .map(x => x.id) // not using Object.keys to avoid unexpected keys and because O(2n) == O(n)
        .reduce((cols, colKey) => {cols[colKey] = initialValues.includes(colKey) ; return cols}, {})
    })
  }
  handleChange(name) {
    this.setState({...this.state, 
      checkedColumns: {
        ...this.state.checkedColumns,
        [name]: !this.state.checkedColumns[name] 
      }
    })
  }
  handleSubmit() {
    const { doSelectOptionalColumns, onClose } = this.props
    const { checkedColumns } = this.state
    const checkedColumnsKeys = Object.keys(checkedColumns)
      .filter(x => checkedColumns.hasOwnProperty(x) && checkedColumns[x])
    doSelectOptionalColumns(checkedColumnsKeys)
    onClose()
  }
  render() {
    const { open, onClose } = this.props
    const { checkedColumns } = this.state
    return (
      <div >
        <Dialog 
          open={open}
          onClose={onClose}
        >
          <DialogTitle>Choose the columns to display</DialogTitle>
          <DialogContent>
            <FormGroup>
              <Grid container>
              {Object.values(FOOD_METADATA).filter(x => x.id !== FOOD_METADATA.name.id).map(x => {
                return (
                  <Grid key={x.id} item xs={12} sm={6} lg={4}>
                    <FormControlLabel 
                      label={x.label}
                      control={
                        <Checkbox
                          checked={checkedColumns[x.id]}
                          onChange={() => this.handleChange(x.id)}
                          color="primary"
                        />
                      }
                    />
                  </Grid>
              )})}
              </Grid>
            </FormGroup>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleReset.bind(this)} color="primary">
              Reset
            </Button>
            <Button onClick={onClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit.bind(this)} color="primary">
              Select
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default connect(
  state => ({
    optionalColumnKeys: state.dictionaryReducer.optionalColumnKeys,
  }),
  (dispatch) => ({
    doSelectOptionalColumns: (optionalColumnKeys) => dispatch(doSelectOptionalColumns(optionalColumnKeys)),
  })
)(withStyles(styles)(SelectColumnsDialog))