import * as Metadata from "../Metadata"

function proxyToBusinessCategory(proxy) {
  const META = Metadata.CATEGORIES_METADATA
  switch (proxy["alim_grp_nom_eng"]) {
    case "beverages":
      return META.beverages.id
    case "cereal products":
      return META.cereals.id
    case "fats and oils":
      return META.fats.id
    case "ice cream and sorbet":
    case "sugar and confectionery":
      return META.sugar.id
    default:
      return undefined
  }
}

export function proxyToBusiness(proxy) {
  const META = Metadata.FOOD_METADATA
  return {
    id: proxy["alim_code"],
    [META.name.id]: proxy["alim_nom_eng"],
    [META.category.id]: proxyToBusinessCategory(proxy),
    [META.energy.id]: proxy["energy"],
    [META.proteins.id]: proxy["mac-proteins"],
    [META.fat.id]: proxy["mac-fat"],
    [META.carbohydrates.id]: proxy["mac-carbohydrates"],
    [META.alcohol.id]: proxy["mac-alcohol"],
    [META.carbs_sugar.id]: proxy["mac-carbohydrates-sugars"],
    [META.fibres.id]: proxy["fibres"],
    [META.fat_saturated.id]: proxy["mac-fat-fa-saturated"],
    [META.fat_fa_n9.id]: proxy["mac-fat-fa-monounsaturated"],
    [META.fat_fa_n6.id]: proxy["mac-fat-fa-n6-18_2"],
    [META.fat_fa_n3.id]: (Number(proxy["mac-fat-fa-n3-18_3"] || 0) + Number(proxy["mac-fat-fa-n3-dha-22_6"] || 0)).toString(),
    [META.min_ca.id]: proxy["min-calcium"],
    [META.min_cl.id]: proxy["min-chloride"],
    [META.min_cu.id]: proxy["min-copper"],
    [META.min_fe.id]: proxy["min-iron"],
    [META.min_i.id]: proxy["min-iodine"],
    [META.min_mg.id]: proxy["min-magnesium"],
    [META.min_mn.id]: proxy["min-manganese"],
    [META.min_p.id]: proxy["min-phosphorus"],
    [META.min_k.id]: proxy["min-potassium"],
    [META.min_se.id]: proxy["min-selenium"],
    [META.min_na.id]: proxy["min-sodium"],
    [META.min_zn.id]: proxy["min-zinc"],
    // missing mo and cr
    [META.provit_a.id]: proxy["provit_a-betacarotene"],
    [META.vit_c.id]: proxy["vit_c"],
    [META.vit_d.id]: proxy["vit_d"],
    [META.vit_e.id]: proxy["vit_e"],
    [META.vit_k.id]: (Number(proxy["vit_k-k1"] || 0) + Number(proxy["vit_k-k2"] || 0)).toString(),
    [META.vit_b1.id]: proxy["vit_b1"],
    [META.vit_b2.id]: proxy["vit_b2"],
    [META.vit_b3.id]: proxy["vit_b3"],
    [META.vit_b5.id]: proxy["vit_b5"],
    [META.vit_b6.id]: proxy["vit_b6"],
    [META.vit_b7.id]: proxy["vit_b7"],
    [META.vit_b9.id]: proxy["vit_b9"],
    [META.vit_b12.id]: proxy["vit_b12"],
    // missing choline
  }
} 
/*
    "provit_a-betacarotene": "8",
    "vit_d": "0.5",
    "": "3.4",
    "": "25.2",
    "": "0.04",
    "": "0.04",
    "": "0.38",
    "": "0.28",
    "": "0.19",
    "": "28",
    "": "0.08"
*/
export function businessToProxy(business) {

}