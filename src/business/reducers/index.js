import { combineReducers } from "redux";

import { reducer as formReducer } from "redux-form"
import foodReducer from "../reducers/FoodReducer"
import dictionaryReducer from "../reducers/DictionaryReducer"


// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #67: redux-form integration
const rootReducer = combineReducers({
  foodReducer: foodReducer,
  dictionaryReducer: dictionaryReducer,
  form: formReducer,
})

export default rootReducer