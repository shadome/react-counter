import * as Actions from "../actions/DictionaryActions"
import { FOOD_METADATA } from "../../business/Metadata"

export const INITIAL_STATE = {
  idCol: FOOD_METADATA.name.id,
  orderDesc: false,
  nbRowsPerPage: 5,
  idPage: 0,
  filterText: undefined,
  optionalColumnKeys: [
    FOOD_METADATA.category.id,
    FOOD_METADATA.energy.id,
    FOOD_METADATA.proteins.id,
    FOOD_METADATA.fat.id,
    FOOD_METADATA.carbohydrates.id,
  ],
}

export default function dictionaryReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Actions.DO_SORT:
      return {...state,
        idCol: action.idCol,
        orderDesc: state.idCol === action.idCol ? !state.orderDesc : false,
      }
    case Actions.DO_CHANGE_PAGE:
      return {...state,
        idPage: action.idPage,
      }
    case Actions.DO_CHANGE_NB_ROWS_PER_PAGE:
      return {...state,
        nbRowsPerPage: action.nbRowsPerPage,
      }
    case Actions.DO_FILTER:
      return {...state,
        filterText: action.filterText,
      }
    case Actions.DO_SELECT_OPTIONAL_COLUMNS:
      let extra = {}
      if (!action.optionalColumnKeys.includes(state.idCol)) {
        extra.idCol = INITIAL_STATE.idCol
        extra.orderDesc = INITIAL_STATE.orderDesc
      }
      return {...state,
        optionalColumnKeys: action.optionalColumnKeys,
        ...extra,
      }
    default:
      return state
  }
}