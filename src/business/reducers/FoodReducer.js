/*
 * Those actions are NOT implemented in a stateless manner,
 * meaning we would rather update this application's state
 * and then propagate any changes into the database accordingly. 
 * Model and business states might not be in perfect sync and
 * are susceptible to cause minor disruptions such as elements
 * reordering on rerender, etc.
 * If a stateless implementation is desired, rerender
 * any value holding data (food, foodList, etc.) by calling
 * corresponding HTTP GET requests whenever a data changing
 * request (PUT, POST, DELETE, etc.) is successfully made (heavier, better sync).
 */

import * as Actions from "../actions/FoodActions"

const INITIAL_STATE = {
  food: undefined,
  foodList: [],
  getFoodPending: false,
  getFoodError: undefined,
  getFoodsPending: false,
  getFoodsError: undefined,
  postFoodPending: false,
  postFoodError: undefined,
  deleteFoodPending: false,
  deleteFoodError: undefined,
  putFoodPending: false,
  putFoodError: undefined,
}

export default function foodReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Actions.GET_FOOD_PENDING:
      return {...state,
        getFoodPending: true,
      }
    case Actions.GET_FOOD_SUCCESS:
      return {...state,
        getFoodPending: false,
        food: action.food
      }
    case Actions.GET_FOOD_FAILURE:
      return {...state,
        getFoodPending: false,
        getFoodError: action.error
      }
    case Actions.GET_FOODS_PENDING:
      return {...state,
        getFoodsPending: true,
      }
    case Actions.GET_FOODS_SUCCESS:
      return {...state,
        getFoodsPending: false,
        foodList: action.foodList
      }
    case Actions.GET_FOODS_FAILURE:
      return {...state,
        getFoodsPending: false,
        getFoodsError: action.error
      }
    case Actions.POST_FOOD_PENDING:
      return {...state,
        postFoodPending: true,
      }
    case Actions.POST_FOOD_FAILURE:
      return {...state,
        postFoodPending: false,
        postFoodError: action.error
      }
    case Actions.POST_FOOD_SUCCESS:
      return {...state,
        postFoodPending: false,
        foodList: [...state.foodList, action.food],
      }
    case Actions.DELETE_FOOD_PENDING:
      return {...state,
        deleteFoodPending: true,
      }
    case Actions.DELETE_FOOD_SUCCESS:
      return {...state,
        foodList: state.foodList.filter(x => x.id !== action.id),
        deleteFoodPending: false,
      }
    case Actions.DELETE_FOOD_FAILURE:
      return {...state,
        deleteFoodPending: false,
        deleteFoodError: action.error,
      }
    case Actions.PUT_FOOD_PENDING:
      return {...state,
        putFoodPending: true,
      }
    case Actions.PUT_FOOD_SUCCESS:
      return {...state,
        putFoodPending: false,
        foodList: state.foodList.map(x => x.id === action.id ? action.food : x), // in hope of maintaining element order
      }
    case Actions.PUT_FOOD_FAILURE:
      return {...state,
        putFoodPending: false,
        putFoodError: action.error,
      }
    default:
      return state
  }
}