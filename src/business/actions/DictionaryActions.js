const STUB = "DICTIONARY_ACTIONS"
const TABLE_STUB = "TABLE"

export const DO_SORT = `${STUB}/${TABLE_STUB}/DO_SORT`
export const DO_CHANGE_PAGE = `${STUB}/${TABLE_STUB}/DO_CHANGE_PAGE`
export const DO_CHANGE_NB_ROWS_PER_PAGE = `${STUB}/${TABLE_STUB}/DO_CHANGE_NB_ROWS_PER_PAGE`
export const DO_FILTER = `${STUB}/${TABLE_STUB}/DO_FILTER`
export const DO_RESET_OPTIONAL_COLUMNS = `${STUB}/${TABLE_STUB}/DO_RESET_OPTIONAL_COLUMNS`
export const DO_SELECT_OPTIONAL_COLUMNS = `${STUB}/${TABLE_STUB}/DO_SELECT_OPTIONAL_COLUMNS`

// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #38+: redux-thunk, middleware for asyn REST API calls


export function doSort(idCol) {
  return {
    type: DO_SORT,
    idCol: idCol,
  }
}

export function doChangePage(idPage) {
  return {
    type: DO_CHANGE_PAGE,
    idPage: idPage,
  }
}

export function doChangeNbRowsPerPage(nbRowsPerPage) {
  return {
    type: DO_CHANGE_NB_ROWS_PER_PAGE,
    nbRowsPerPage: nbRowsPerPage,
  }
}

export function doFilter(filterText) {
  return {
    type: DO_FILTER,
    filterText: filterText,
  }
}

export function doSelectOptionalColumns(optionalColumnKeys) {
  return {
    type: DO_SELECT_OPTIONAL_COLUMNS,
    optionalColumnKeys: optionalColumnKeys
  }
}