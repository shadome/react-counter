import * as FoodServices from "../../proxies/FoodServices"

const STUB = "FOOD_ACTIONS"

export const GET_FOOD_PENDING = `${STUB}/GET_FOOD_PENDING`
export const GET_FOOD_SUCCESS = `${STUB}/GET_FOOD_SUCCESS`
export const GET_FOOD_FAILURE = `${STUB}/GET_FOOD_FAILURE`
export const GET_FOODS_PENDING = `${STUB}/GET_FOODS_PENDING`
export const GET_FOODS_SUCCESS = `${STUB}/GET_FOODS_SUCCESS`
export const GET_FOODS_FAILURE = `${STUB}/GET_FOODS_FAILURE`
export const POST_FOOD_PENDING = `${STUB}/POST_FOOD_PENDING`
export const POST_FOOD_SUCCESS = `${STUB}/POST_FOOD_SUCCESS`
export const POST_FOOD_FAILURE = `${STUB}/POST_FOOD_FAILURE`
export const DELETE_FOOD_PENDING = `${STUB}/DELETE_FOOD_PENDING`
export const DELETE_FOOD_SUCCESS = `${STUB}/DELETE_FOOD_SUCCESS`
export const DELETE_FOOD_FAILURE = `${STUB}/DELETE_FOOD_FAILURE`
export const PUT_FOOD_PENDING = `${STUB}/PUT_FOOD_PENDING`
export const PUT_FOOD_SUCCESS = `${STUB}/PUT_FOOD_SUCCESS`
export const PUT_FOOD_FAILURE = `${STUB}/PUT_FOOD_FAILURE`

// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #38+: redux-thunk, middleware for asyn REST API calls

export function initFood(id, reduxFormCallback) {
  return () => 
    FoodServices.getFood(id)
      .then((response) => reduxFormCallback(response))
}

export function getFood(id) {
  return (dispatch) => {
    dispatch({
      type: GET_FOOD_PENDING
    })
    return FoodServices.getFood(id)
        .then((response) => {
          dispatch({
            type: GET_FOOD_SUCCESS,
            food: response,
          })
        })
  }
}

export function getFoods() {
  return (dispatch) => {
    dispatch({
      type: GET_FOODS_PENDING
    })
    return FoodServices.getFoods()
        .then((response) => {
          dispatch({
            type: GET_FOODS_SUCCESS,
            foodList: response,
          })
        })
  }
}

export function postFood(food) {
  return (dispatch) => {
    dispatch({
      type: POST_FOOD_PENDING
    })
    return FoodServices.postFood(food)
        .then(() => {
          dispatch({
            food: food,
            type: POST_FOOD_SUCCESS,
          })
        })
  }
}

export function deleteFood(id) {
  return (dispatch) => {
    dispatch({
      type: DELETE_FOOD_PENDING,
      id: id,
    })
    return FoodServices.deleteFood(id)
        .then(() => {
          dispatch({
            type: DELETE_FOOD_SUCCESS,
            id: id,
          })
        })
  }
}

export function putFood(id, food) {
  return (dispatch) => {
    dispatch({
      type: PUT_FOOD_PENDING
    })
    return FoodServices.putFood(id, food)
        .then(() => {
          dispatch({
            type: PUT_FOOD_SUCCESS,
            id: id,
            food: food
          })
        })
  }
}