// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #52+: use of json-server, run json-server --watch db.json --port 4000
const API_ENDPOINT = "http://localhost:4000"
// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #16: REST call (get)
export async function getFood(id) {
  return fetch(`${API_ENDPOINT}/foods/${id}`, {method: "GET"})
    .then(response => response.json())
}

export async function getFoods() {
  return fetch(`${API_ENDPOINT}/foods`, {method: "GET"})
    .then(response => response.json())
}

// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #16: REST call (post)
export async function postFood(food) {
  return fetch(`${API_ENDPOINT}/foods`, {
      "body": JSON.stringify(food),
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      "method": "POST"
    }
  ).then(response => response.json())
}

export async function deleteFood(id) { 
  return fetch(`${API_ENDPOINT}/foods/${id}`, { method: "DELETE" })
    .then(response => response.json())
}

export async function putFood(id, food) {
  return fetch(`${API_ENDPOINT}/foods/${id}`, {
      "body": JSON.stringify(food),
      "headers": {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      "method": "PUT"
    }
  ).then(response => response.json())
}