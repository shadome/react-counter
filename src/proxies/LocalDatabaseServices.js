import { CiqualEN } from "../data/CiqualEN"

// TODO if more local data sources, let the caller choose between sources
// may also be boilerplate if the components using this data handle sorting and filtering themselves

// TODO handle the datasource id parameter (see DATASOURCES_METADATA)
export function searchEntries(string = "") {
  let s = string.trim().toLowerCase()
  if (!s) // optional string parameter: search all entries if none
    return CiqualEN.map((item) => {return {...item, id:item.alim_code, name:item.alim_nom_eng}})
  // if (s.length < 3) // search triggered starting at 3 meaningful chars
  //   return undefined
  return CiqualEN
    .filter(x => 
      x.alim_nom_eng
        .split(",")[0]  // only keeping the string before first comma, the rest is descriptive ("cooked", "raw", etc.)
        .toLowerCase()
        .includes(s)
    )
    // .reduce(function (filtered, entry) {
    //   if (entry.alim_nom_eng.toLowerCase().includes(s)) {
    //     filtered.push({id:entry.alim_code, name:entry.alim_nom_eng})
    //   }
    //   return filtered
    // }, [])
    .sort(function(a, b) {return a.alim_nom_eng.length - b.alim_nom_eng.length}) // formatting function, TODO? maybe put into presentation layer
}

export function getEntry(id) {
  return CiqualEN.find(x => x.alim_code === id)
}