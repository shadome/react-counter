import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom"

import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import rootReducer from "./business/reducers";

import NavigationBar from "./presentation/components/NavigationBar"
import ListFoodDictionaryPage, {ListFoodDictionaryPath} from "./presentation/pages/ListFoodDictionary/ListFoodDictionaryPage"
import RegisterFoodPage, {RegisterFoodPath} from "./presentation/pages/RegisterFood/RegisterFoodPage"

// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #41+: redux middleware async support (redux-thunk)
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore)

// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #26+: redux
// https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #55+: react-router (was out of date: used react-router-dom instead)
export default class App extends Component {
  render() {
    // https://www.udemy.com/react-redux-tutoriel-pour-debutants-en-francais/ #37: devtools support
    const devtools_extension_support = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    return (
      <Provider store={createStoreWithMiddleware(rootReducer, devtools_extension_support)}>
        <BrowserRouter>
          <div className="App">
            <NavigationBar/>
            <Switch>
              <Route path={`${RegisterFoodPath}/:id?`} component={RegisterFoodPage} />
              {/* TODO manage form prefilling with existing data to handle modification and creation with the same component */}
              {/* <Route path={`${RegisterFoodPath}/:id`} component={RegisterFoodPage} /> */}
              <Route path="/foods/:id" component={undefined} /> {/* TODO */}
              <Route path={`${ListFoodDictionaryPath}`} component={ListFoodDictionaryPage} />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}
